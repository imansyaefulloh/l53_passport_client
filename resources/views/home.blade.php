@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Timeline</div>

                <div class="panel-body">
                    @if (Auth::user()->token)
                        @if ($tweets->count())
                            @foreach ($tweets as $tweet)
                                <div class="media">
                                    <div class="media-left">
                                        <a href="">
                                            <img src="http://place-hold.it/64x64" alt="">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <strong>{{ $tweet->user->name }}</strong>
                                        <p>{{ $tweet->body }}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @else
                        Please <a href="{{ url('/auth/twitter') }}">authorize with twitter</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
